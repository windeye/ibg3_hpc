#!/bin/csh                                                                                                                                                           

setenv CCSMROOT $HOME/cesm1_2_2

foreach nprocs ( 24 )
setenv CASE jureca_I_test_$nprocs
setenv USER_EMAIL g.he@fz-juelich.de

cd $CCSMROOT/scripts
rm $CASE -fr
./create_newcase -case $CASE \
                 -res f09_g16  \
                 -compset I_2000_CLM45 \
                 -mach jureca \
                 -compiler gnu
cd $CASE

setenv NPROCS $nprocs


./xmlchange -file env_mach_pes.xml -id NTASKS_LND -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_ATM -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_ICE -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_WAV -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_CPL -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_GLC -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_OCN -val $NPROCS
./xmlchange -file env_mach_pes.xml -id NTASKS_ROF -val $NPROCS
./xmlchange -file env_mach_pes.xml -id MAX_TASKS_PER_NODE -val 24


./xmlchange -file env_run.xml -id STOP_OPTION -val "nsteps"
./xmlchange -file env_run.xml -id STOP_N -val "6"
./xmlchange -file env_run.xml -id REST_N -val "2"

./cesm_setup

./$CASE.build
end
