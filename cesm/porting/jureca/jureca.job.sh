#!/bin/bash -x
#SBATCH --nodes=1
#SBATCH --ntasks=24
#SBATCH --ntasks-per-node=24
#SBATCH --output=mpi-out.%j
#SBATCH --error=mpi-err.%j
#SBATCH --time=00:15:00
#SBATCH --partition=batch
source ~/scripts/switch_gnu_toolchain.sh
RUN_DIRECTORY=/work/jicg41/jicg4146/CESM/jureca_I_test_24/run
EXE_DIRECTORY=/work/jicg41/jicg4146/CESM/jureca_I_test_24/exe
cd $RUN_DIRECTORY # This is where your inputs of CESM are, e.g., lnd_in, atm_in and drv_in....
srun $EXE_DIRECTORY/cesm.exe # This is where your cesm.exe locates
